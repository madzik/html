function doFirst(){
	barSize=600;
	myMovie=document.getElementById('myMovie');
	playButton=document.getElementById('playButton');
	bar=document.getElementById('defaultBar');
	progressBar=document.getElementById('progressBar');
/* wez przycisk playButton i gdy kliknie włącza funkcje playOrPause
następnie wyłacz w postaci false*/
	playButton.addEventListener('click',playOrPause, false);
	bar.addEventListener('click',clickedBar, false);
}
/*jeśli film nie jest zapauzowany i nie jest skończony wykonaj*/
function playOrPause(){
	if(!myMovie.pause && !myMovie.ended){
		myMovie.pause();
		/*zmienna playButton dostęp ma do zmiennej wewnętrznje HTML
		następnie ustawiamy dostep do tesktu przycisku
		by wykonać to co chcemy zrobić  */
		playButton.innerHTML='Play';
		/*ustawienie na 90 sekund*/
		window.clearInterval(updateBar);
	}else{
		myMovie.play();
		playButton.innerHTML="Pause";
		updateBar=setInterval(upadate, 500);
	}
}

function update(){
	if(!myMovie.ended){
		var size=parseInt(myMovie.currentTime*barSize/myMovie.duration);
		progressBar.style.width=size+'px';
	}else{
		progressBar.style.width='0px';
		playButton.innerHTML="Play";
		window.clearInterval(updateBar);
	}
}
function clickedBar(e){
	if(!myMovie.paused && !myMovie.ended ){
		/*wykonaj mouseX, w której będzie przesuniecie
		z miejsca X odjąc ustawienie paska po lewej
		stronie i ustaw nowy czas */
		var mouseX= e.pageX-bar.offsetLeft;	
		var newtime = mouseX*myMovie.duration/barSize;
		myMovie.currentTime=newtime;
		progressBar.style.width=mouseX+'px';

	}
}
window.addEventListener('load',doFirst, false);